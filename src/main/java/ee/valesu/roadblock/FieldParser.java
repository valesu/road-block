package ee.valesu.roadblock;

public class FieldParser {

    public Field parse(String str) {
        String[] rows = str.split("\n");

        int width = removeSpaces(rows[0]).length();
        int height = rows.length;

        Field field = new Field(width, height);

        for (int y = 0; y < rows.length; y++) {
            char[] chars = removeSpaces(rows[y]).toCharArray();
            for (int x = 0; x < chars.length; x++) {
                field.set(x, y, PointType.lookup(chars[x]));
            }
        }

        return field;
    }

    private String removeSpaces(String str) {
        return str.replaceAll("\\s", "");
    }
}
