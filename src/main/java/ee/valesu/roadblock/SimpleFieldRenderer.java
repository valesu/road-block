package ee.valesu.roadblock;

public class SimpleFieldRenderer {

    public String render(Field field) {
        StringBuilder sb = new StringBuilder();

        for (int y = 0; y < field.getHeight(); y++) {
            for (int x = 0; x < field.getWidth(); x++) {
                sb.append(field.get(x, y).getChr())
                        .append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }
}
