package ee.valesu.roadblock;

import java.util.List;

import static ee.valesu.roadblock.PointType.*;

public class BruteForceSolver {

    public Solution solve(Field field, List<Field> figures) {
        return iterate(field, figures, 0);
    }

    private Solution iterate(Field field, List<Field> figures, int figureIndex) {
        Field figure = figures.get(figureIndex);

        while (figure.getRotation() < 4) {
            for (int x = 0; x <= field.getWidth() - figure.getWidth(); x++) {
                for (int y = 0; y <= field.getHeight() - figure.getHeight(); y++) {
                    Field newField = fit(field, figure, x, y);

                    // Check if puzzle solved when figure fits
                    if (newField != null) {
                        if (solved(newField)) {
                            Solution solution = new Solution(newField);
                            solution.add(new Move(figure, x, y));
                            return solution;
                        } else if (figureIndex < figures.size() - 1) {
                            Solution solution = iterate(newField, figures, figureIndex + 1);

                            if (solution != null) {
                                solution.add(new Move(figure, x, y));
                                return solution;
                            }
                        }
                    }
                }
            }

            figure = figure.rotate();
        }

        return null;
    }

    /**
     * Checks if puzzle is solved. Puzzle is considered solved if all places are filled and red car has no routes to escape.
     *
     * @param field
     * @return true if puzzle is solved
     */
    private boolean solved(Field field) {
        int rx = -1;
        int ry = -1;

        // first stage: check if there are empty fields
        // additionally find a red car
        for (int x = 0; x < field.getWidth(); x++) {
            for (int y = 0; y < field.getHeight(); y++) {
                if (field.get(x, y) == EMPTY) {
                    return false;
                }

                if (field.get(x, y) == RED_CAR) {
                    rx = x;
                    ry = y;
                }
            }
        }

        // second stage: check if there is escape route
        if (rx < 0 || ry < 0) {
            throw new RuntimeException("No red car?: " + field.toString());
        }

        return !canEscape(field);
    }

    private boolean canEscape(Field originalField) {
        Field fieldCopy = originalField.copy();
        boolean atLeastOnePointSet;
        do {
            atLeastOnePointSet = false;
            for (int x = 0; x < fieldCopy.getWidth(); x++) {
                for (int y = 0; y < fieldCopy.getHeight(); y++) {
                    if (fieldCopy.get(x, y) == RED_CAR) {
                        // red car at the border means that it can escape
                        if (x == 0 || x == fieldCopy.getWidth() - 1
                                || y == 0 || y == fieldCopy.getHeight() - 1) {
                            return true;
                        }

                        // mark nearest point as red cars
                        if (fieldCopy.get(x, y) == RED_CAR) {
                            atLeastOnePointSet |= setPointIfPossible(fieldCopy, x - 1, y, RED_CAR);
                            atLeastOnePointSet |= setPointIfPossible(fieldCopy, x + 1, y, RED_CAR);
                            atLeastOnePointSet |= setPointIfPossible(fieldCopy, x, y - 1, RED_CAR);
                            atLeastOnePointSet |= setPointIfPossible(fieldCopy, x, y + 1, RED_CAR);
                        }
                    }
                }
            }
        } while (atLeastOnePointSet);

        return false;
    }

    private boolean setPointIfPossible(Field field, int x, int y, PointType pointType) {
        if (x >= 0 && x < field.getWidth()
                && y >= 0 && y < field.getHeight()
                && (field.get(x, y) == FILLED || field.get(x, y) == EMPTY)) {
            field.set(x, y, pointType);
            return true;
        }

        return false;
    }


    /**
     * Fits figure to field
     *
     * @param field
     * @param figure
     * @param x
     * @param y
     * @return new field if figure fits, null otherwise
     */
    private Field fit(Field field, Field figure, int x, int y) {
        Field newField = field.copy();

        for (int fx = 0; fx < figure.getWidth(); fx++) {
            for (int fy = 0; fy < figure.getHeight(); fy++) {
                if (figure.get(fx, fy) != EMPTY) {
                    if (newField.get(x + fx, y + fy) != EMPTY) {
                        return null;
                    }

                    newField.set(x + fx, y + fy, figure.get(fx, fy));
                }
            }
        }

        return newField;
    }

}
