package ee.valesu.roadblock;

import java.util.HashMap;
import java.util.Map;

public enum PointType {
    EMPTY('E'),
    BUILDING('B'),
    RED_CAR('R'),
    POLICE_CAR('P'),
    FILLED('F');

    private static Map<Character, PointType> lookup = new HashMap<>();

    static {
        for (PointType pointType : PointType.values()) {
            lookup.put(pointType.getChr(), pointType);
        }
    }

    private final char chr;

    PointType(char chr) {
        this.chr = chr;
    }

    public static PointType lookup(char chr) {
        assert lookup.containsKey(chr);
        return lookup.get(chr);
    }

    public char getChr() {
        return chr;
    }
}
