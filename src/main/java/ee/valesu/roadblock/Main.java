package ee.valesu.roadblock;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private final FieldParser parser = new FieldParser();
    private final SimpleFieldRenderer fieldRenderer = new SimpleFieldRenderer();
    private final SolutionRenderer solutionRenderer = new SolutionRenderer();
    private final BruteForceSolver solver = new BruteForceSolver();

    public Main() {
        Field field = initField80();
        List<Field> figures = initFigures();

        System.out.println("Field:");
        System.out.println(fieldRenderer.render(field));

        System.out.println("Figures:");
        figures.forEach(fig -> System.out.println(fieldRenderer.render(fig)));

        List<Solution> solutions = new ArrayList<>();
        solutions.add(solver.solve(field, figures));

        if (solutions.isEmpty()) {
            System.out.println("No solution found");
        } else {
            for (int solutionIndex = 0; solutionIndex < solutions.size(); solutionIndex++) {
                System.out.println("Solution " + solutionIndex + ":");
                System.out.println(solutionRenderer.render(solutions.get(solutionIndex)));
            }
        }
    }

    public static void main(String[] args) {
        new Main();
    }

    private void solve(Field field, List<Field> figures) {

    }

    private List<Field> initFigures() {
        List<Field> figures = new ArrayList<>();


        figures.add(parser
                .parse("F P\n" +
                        "E F"));

        figures.add(parser
                .parse("F F P\n" +
                        "E E F"));

        figures.add(parser
                .parse("F P F"));

        figures.add(parser
                .parse("F F\n" +
                        "E P"));

        figures.add(parser
                .parse("F F F\n" +
                        "E P E"));

        figures.add(parser
                .parse("F F P\n" +
                        "F E E"));


        return figures;
    }

    private Field initFieldEmpty() {
        return parser
                .parse("E E E E E E\n" +
                        "E E E E E E\n" +
                        "E E E E E E\n" +
                        "E E E E E E\n" +
                        "E E E E E E\n" +
                        "E E E E E E");
    }

    private Field initField77() {
        return parser
                .parse(" B B B B E E\n" +
                        "B B B E E E\n" +
                        "B B E E E E\n" +
                        "B B R E E E\n" +
                        "B E E E E E\n" +
                        "B B E E E E");
    }

    private Field initField14() {
        return parser
                .parse("B B B B B E\n" +
                        "E E E B R E\n" +
                        "B B E B E E\n" +
                        "B E E B E E\n" +
                        "B E E B B E\n" +
                        "E E E E E E");
    }

    private Field initField50() {
        return parser
                .parse("B B B B E E\n" +
                        "B B B E E E\n" +
                        "B B R E E E\n" +
                        "B B E E E E\n" +
                        "B E E E E E\n" +
                        "B B E E E E");
    }

    private Field initField80() {
        return parser
                .parse("B B B E E E\n" +
                        "E E E E E E\n" +
                        "E E E E E E\n" +
                        "B B E R E E\n" +
                        "B B E E E B\n" +
                        "B B B B B B");
    }

}
