package ee.valesu.roadblock;

import lombok.Getter;
import lombok.ToString;

import java.util.Arrays;

@ToString
public class Field {

    private PointType[][] field;

    @Getter
    private int width;

    @Getter
    private int height;

    @Getter
    private int rotation = 0;

    public Field(int width, int height) {
        this.height = height;
        this.width = width;
        reset();
    }

    public void reset() {
        field = new PointType[height][width];
        for (int y = 0; y < height; y++) {
            Arrays.fill(field[y], PointType.EMPTY);
        }
    }

    public PointType get(int x, int y) {
        assert x >= 0 && x < width;
        assert y >= 0 && y < height;

        return field[y][x];
    }

    public Field copy() {
        Field copy = new Field(width, height);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                copy.set(x, y, get(x, y));
            }
        }

        copy.rotation = this.rotation;

        return copy;
    }

    public Field rotate() {
        Field rotated = new Field(height, width);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                rotated.set(height - y - 1, x, get(x, y));
            }
        }

        rotated.rotation = this.rotation + 1;

        return rotated;
    }

    public void set(int x, int y, PointType value) {
        field[y][x] = value;
    }

    public String toString() {
        return new SimpleFieldRenderer().render(this);
    }

}
