package ee.valesu.roadblock;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    @Getter
    private Field solvedField;

    @Getter
    private List<Move> moves = new ArrayList<>();

    public Solution(Field solvedField) {
        this.solvedField = solvedField;
    }

    public void add(Move move) {
        moves.add(move);
    }
}
