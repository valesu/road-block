package ee.valesu.roadblock;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Move {

    private Field figure;
    private int x;
    private int y;

}
