package ee.valesu.roadblock;

public class SolutionRenderer {

    public SolutionRenderer() {
    }

    public String render(Solution solution) {
        Field solvedField = solution.getSolvedField();
        char[][] field = new char[solvedField.getHeight()][solvedField.getWidth()];

        for (int y = 0; y < solvedField.getHeight(); y++) {
            for (int x = 0; x < solvedField.getWidth(); x++) {
                field[y][x] = solvedField.get(x, y).getChr();
            }
        }

        for (int moveIndex = 0; moveIndex < solution.getMoves().size(); moveIndex++) {
            Move move = solution.getMoves().get(moveIndex);
            Field moveFigure = move.getFigure();

            for (int figureX = 0; figureX < moveFigure.getWidth(); figureX++) {
                for (int figureY = 0; figureY < moveFigure.getHeight(); figureY++) {
                    PointType moveFigurePoint = moveFigure.get(figureX, figureY);

                    int fieldY = move.getY() + figureY;
                    int fieldX = move.getX() + figureX;

                    if (moveFigurePoint == PointType.POLICE_CAR) {
                        field[fieldY][fieldX] = moveFigurePoint.getChr();
                    } else if (moveFigurePoint == PointType.FILLED) {
                        field[fieldY][fieldX] = Character.forDigit(moveIndex, 10);
                    }
                }
            }
        }

        StringBuilder sb = new StringBuilder();

        for (int y = 0; y < field.length; y++) {
            for (int x = 0; x < field[y].length; x++) {
                sb.append(field[y][x]).append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }
}
