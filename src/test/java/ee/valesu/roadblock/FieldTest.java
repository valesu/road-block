package ee.valesu.roadblock;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class FieldTest {

    private Field field = new FieldParser()
            .parse("B P R\n" +
                    "F E P");

    @Test
    public void changesDimensionsDuringRotation() {
        Field rotated = field.rotate();

        assertThat(rotated.getWidth(), equalTo(2));
        assertThat(rotated.getHeight(), equalTo(3));
    }

    @Test
    public void rotatesField() {
        Field rotated = field.rotate();

        assertThat(rotated.get(0, 0), equalTo(PointType.FILLED));
        assertThat(rotated.get(1, 0), equalTo(PointType.BUILDING));
        assertThat(rotated.get(0, 1), equalTo(PointType.EMPTY));
        assertThat(rotated.get(1, 1), equalTo(PointType.POLICE_CAR));
        assertThat(rotated.get(0, 2), equalTo(PointType.POLICE_CAR));
        assertThat(rotated.get(1, 2), equalTo(PointType.RED_CAR));
    }

    @Test
    public void copiesField() {
        Field copy = field.copy();

        assertThat(copy, is(not(field)));
        assertThat(copy.get(0, 0), equalTo(PointType.BUILDING));
        assertThat(copy.get(1, 0), equalTo(PointType.POLICE_CAR));
        assertThat(copy.get(2, 0), equalTo(PointType.RED_CAR));
        assertThat(copy.get(0, 1), equalTo(PointType.FILLED));
        assertThat(copy.get(1, 1), equalTo(PointType.EMPTY));
        assertThat(copy.get(2, 1), equalTo(PointType.POLICE_CAR));
    }

    @Test
    public void incrementsRotation() {
        Field rotated = field.rotate();

        assertThat(rotated.getRotation(), equalTo(1));
    }

    @Test
    public void resetsRotationAfterFourTurns() {
        field.rotate();
        field.rotate();
        field.rotate();
        field.rotate();

        assertThat(field.getRotation(), equalTo(0));
    }

    @Test
    public void copiesRotationValue() {
        Field rotated = field.rotate();
        Field copy = rotated.copy();

        assertThat(copy.getRotation(), equalTo(1));
    }

}